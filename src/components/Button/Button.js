import "./Button.module.css";

export default function Button({ numeroDelBoton, handleClick }) {
  const label = numeroDelBoton > 1 ? "dice" : "die";

  return (
    <div className="Button">
      <button type="button" onClick={() => handleClick(numeroDelBoton)}>
        {numeroDelBoton} {label}
      </button>
    </div>
  );
}
